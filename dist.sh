#!/usr/bin/env bash
. `dirname "$0"`/sh/init.sh
rm -rf public
npx yarn run build

./sh/html2txt.ls

git checkout dist/package.json

cd dist/
npm version patch
npm publish
git add -u
git commit -m "dist"
