#!/usr/bin/env bash
. `dirname "$0"`/sh/init.sh

cd $ROOT
if [ ! \( -e "./public/6.js" \) ]; then
  print "init public"
  mkdir -p public
  cd public
  6du -y -d .
  cat $ROOT/sh/6.js > 6du/6.js
  6du
fi

npx yarn run serve

