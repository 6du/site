import
  \@/ls/spin/init.ls : Spin

export default _ = ~>
  Spin.inc!
  arguments[0] = C.cdn.site+'/'+arguments[0]
  try
    r = await fetch.apply(void, arguments)
    t = await r.text()
  finally
    Spin.done!
  return t
