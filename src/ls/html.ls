export default _ = (render, mod)->
  html = require("@/ls/html/#{render}.ls").default
  beforeRoute = mod.beforeRoute

  mod.beforeRoute = (to, from, next)->
    body = html(await $f(render+"/"+(to.params.f or 'index')))
    insert = !->
      @$refs.m.innerHTML = body
    if beforeRoute
      beforeRoute(
        to
        from
        (func)!~>
          next !->
            insert.apply @, arguments
            func.apply @, arguments
      )
    else
      next insert
  return mod

